# Install Script
This repository contains a single bash script that will install all the 
verification and formal verification dependencies of all our up and coming projects.

This includes:
- verilator
- gtk-wave
- symbiyosys
- symbiyosys solvers (z3, yices...)

For more details, please see their websites:
- Verilator: https://www.veripool.org/projects/verilator/wiki/Installing
- Symbiyosys: https://symbiyosys.readthedocs.io/en/latest/install.html#yosys-yosys-smtbmc-and-abc

# Usage
To use this script, simply make this script an executable by:

`$ sudo chmod +x install.sh`

Then, execute the script in the desired directory as root:

`$ sudo ./install.sh`

Note that a file called `dependencies` will be created at the running directory with relevant
repositories cloned into this folder. Please make sure you copy the script and run the script 
at the directory you want this folder to be created.

# Tested Platform
This script is only tested on PopOS 20.10 at the moment.

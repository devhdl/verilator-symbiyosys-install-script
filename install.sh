#!/usr/bin/bash

echo -e "Please run this file with root permission!"
echo ""

echo "We will install the dependencies of the upcoming HDL projects."
echo "This includes:"
echo "- Verilator"
echo "- gtk-wave"
echo "- symbiyosys and its solvers (z3, yices)"
echo ""

echo "A directory called dependencies will be made in the current directory."
echo "If you wish to install in a different directory, please copy this script"
echo "in that directory before continuing!"

read -p "Would you like to continue? [Y/n]" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    # do dangerous stuff
    echo ""
    echo "Installing Verilator:"
    apt install verilator
    echo "Verilator successfully installed!"
    echo ""

    echo "Installing gtk-wave:"
    apt install gtkwave
    echo "gtk-wave successcully installed!"
    echo ""

    echo "Making directory dependencies."
    mkdir dependencies
    cd dependencies
    echo ""
    echo "Installing SymbiYosys dependencies:"
    apt-get install build-essential clang bison flex libreadline-dev \
                     gawk tcl-dev libffi-dev git mercurial graphviz   \
                     xdot pkg-config python python3 libftdi-dev gperf \
                     libboost-program-options-dev autoconf libgmp-dev \
                     cmake
    echo ""
    echo "Installing yosys, yosys-smtbmc, yosys-abc:"
    git clone https://github.com/YosysHQ/yosys.git yosys
    cd yosys
    make -j$(nproc)
    sudo make install
    cd ..

    echo ""
    echo "Installing Symbiyosys:"
    git clone https://github.com/YosysHQ/SymbiYosys.git SymbiYosys
    cd SymbiYosys
    sudo make install
    cd ..

    echo ""
    echo "Installing Yices2:"
    git clone https://github.com/SRI-CSL/yices2.git yices2
    cd yices2
    autoconf
    ./configure
    make -j$(nproc)
    sudo make install
    cd ..

    echo ""
    echo "Installing Z3:"
    git clone https://github.com/Z3Prover/z3.git z3
    cd z3
    python scripts/mk_make.py
    cd build
    make -j$(nproc)
    sudo make install
    cd ..
    cd ..

    echo ""
    echo "Installation finished!!"
fi
